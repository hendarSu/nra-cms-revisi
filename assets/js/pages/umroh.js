/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {

    var initValidationUpdate = function() {
        jQuery('.form-update-umroh').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                title: {
                    required: true,
                },
                subtitle: {
                    required: true,

                },
                content: {
                  required: true,

                },

            },
            messages: {
                title: {
                    required: 'Please enter a title',
                },
                subtitle: {
                    required: 'Please enter a subtitle',
                },
              content: {
                    required: 'Please enter a title',
                },



            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {

                            window.location.reload();
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Save');
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Save');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {
            // Init Datatables
            initValidationUpdate();

        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});
