/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableMember = function() {
        window.tableMember = jQuery('.data-member').dataTable({
            order: [[1, 'asc']],
            columnDefs: [{ orderable: false, targets: [0, 1] }],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
            processing: true,
            serverSide: false,
            ajax: '/members/get',
            columns: [
                  { data: 'fullname' },
                        { data: 'sex' },
                        { data: 'address'},
                        { data: 'status' },


                          { data: 'id',
                            render: function(data, type, row) {
                                var str = '<button class="btn btn-success" onClick="openModal(\'#modal-update-member\', \'update\', \'' + data + '\')" data-toggle="tooltip" title="Approve member">Approve</button>';
                                return str;
                            },
                          },
                        

                  ],
        }).on('draw.dt', function() {
            $(this).removeAttr('style');
            $('[data-toggle="tooltip"]').tooltip();
            $('.checkbox-member').shiftcheckbox({
                checkboxSelector: ':checkbox',
                selectAll: $('.checkbox-member-all'),
                ignoreClick: 'a',
                onChange: function(checked) {

                },
            });
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend(true, $DataTable.defaults, {
            dom:
                '<\'row\'<\'col-sm-6\'l><\'col-sm-6\'f>>' +
                '<\'row\'<\'col-sm-12\'tr>>' +
                '<\'row\'<\'col-sm-6\'i><\'col-sm-6\'p>>',
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: '_MENU_',
                sInfo: 'Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>',
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>',
                },
            },
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: 'dataTables_wrapper form-inline dt-bootstrap',
            sFilterInput: 'form-control',
            sLengthSelect: 'form-control',
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function(settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function(container, buttons) {
                var i, ien, node, button;
                var clickHandler = function(e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    } else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                class: classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                tabindex: settings.iTabIndex,
                                id: idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null,
                            })
                            .append(jQuery('<a>', {
                                    href: '#',
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                container: 'DTTT btn-group',
                buttons: {
                    normal: 'btn btn-default',
                    disabled: 'disabled',
                },
                collection: {
                    container: 'DTTT_dropdown dropdown-menu',
                    buttons: {
                        normal: '',
                        disabled: 'disabled',
                    },
                },
                print: {
                    info: 'DTTT_print_info',
                },
                select: {
                    row: 'active',
                },
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                collection: {
                    container: 'ul',
                    button: 'li',
                    liner: 'a',
                },
            });
        }
    };

    var initValidationAdd = function() {
        jQuery('.form-add-member').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                question: {
                    required: true,
                },
                answer: {
                    required: true,

                },
            },
            messages: {
                 question: {
                    required: 'Please enter a question',
                },
                answer: {
                    required: 'Please enter a answer',
                },

            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();
                            $('#imgPreview').attr('src', '/img/default.png')
                            $('#modal-add-member').modal('hide');
                            window.tableMember.api().ajax.reload();
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Save');
                    },

                    error: function(jqXHR, exception) {
                      console.log(jqXHR)
                        $('#modal-notif .block-content p').text(jqXHR.responseJSON.data);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Save');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    var initValidationUpdate = function() {
        jQuery('.form-update-member').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                namaBank: {
                    required: true,
                },
                namaRekening: {
                    required: true,

                },
                nomerRekening: {
                    required: true,
                    number: true,

                },
                currencyRekening: {
                    required: true,
                },
            },
            messages: {
                 namaBank: {
                    required: 'Please enter a Bank name',
                },
                namaRekening: {
                    required: 'Please enter a rekening name',
                },
                nomerRekening: {
                    required: 'Please enter a rekening number',
                },
               currencyRekening: {
                    required: 'Please enter a currency',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        if (res.data == '1') {
                            form.reset();
                            $('#imgPreview-update').attr('src', '/img/default.png')
                            $('#modal-update-member').modal('hide');
                            window.tableMember.api().ajax.reload();
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                        }
                        button.removeAttr('disabled');
                        button.text('Save');
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Save');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    return {
        init: function() {
            // Init Datatables
            initValidationAdd();
            initValidationUpdate();
            bsDataTables();
            initDataTableMember();
        },
    };
}();

// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
});

/* PROCCESSING */

// open modal add/update
function openModal(target, type, id) {
    if (type == 'update') {
        $.get('/member/get/'+id, function(res) {
            var member = res.data;
            $('.form-update-member').attr('action', '/member/update/'+id);
            $('#question-update-member').val(member.question);
            $('#answer-update-member').val(member.answer);

        });
    }

    $(target).modal('show');
}

// add
$('input.upload').on('change', function(e) {
    if (this.files && this.files[0].name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG)$/)) {
        var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
        var reader = new FileReader();
        reader.onload = function(e) {
            image.attr('src', e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
        $('#cropper-modal').modal('show');
    }else {
        alert('file not supported');
    }
});

$('#cropper-modal').on('shown.bs.modal', function() {
    var image = $('#cropper-wrap-img > img'), cropBoxData, canvasData;
    image.cropper({
        aspectRatio: 1 / 1,
        autoCropArea: 0.5,
        cropBoxResizable: true,
        checkImageOrigin: true,
        responsive: true,
        built: function() {
            // Strict mode: set crop box data first
            image.cropper('setCropBoxData', cropBoxData);
            image.cropper('setCanvasData', canvasData);
        },
    });
});


$('.btn-crop').on('click', function(e) {
    var imgb64 = $('#cropper-wrap-img > img').cropper('getCroppedCanvas').toDataURL('image/jpeg');
    $('img#imgPreview').attr('src', imgb64);
    $('#srcDataCrop').val(imgb64);
    $('img#imgPreview-update').attr('src', imgb64);
    $('#srcDataCrop-update').val(imgb64);
    $('#cropper-modal').modal('hide');
});

$('#cropper-modal').on('hidden.bs.modal', function() {
    $('#cropper-wrap-img > img').cropper('destroy');
    $('body').addClass('modal-open');
});

$('#modal-add-member').on('hidden.bs.modal', function() {
    $('img#imgPreview').attr('src', '/img/default.png');
    $('#srcDataCrop').val('');
});

$('#modal-update-member').on('hidden.bs.modal', function() {
    $('img#imgPreview-update').attr('src', '/img/default.png');
    $('#srcDataCrop-update').val('');
});

// remove
$(document).on('change', '.data-member input:checkbox', function() {
    if ($('.data-member input:checkbox:checked').length > 0) {
        $('.btn-delete-member').removeAttr('disabled');
    }else {
        $('.btn-delete-member').attr('disabled', 'disabled');
    }
});

$('.btn-delete-member').click(function(e) {
    if ($('.data-member input:checkbox:checked').length > 0) {
        var conf = confirm('Remove selected member(s) ?');
        if (conf) {
            var data = $('.data-member input:checkbox:checked').serialize();

            $.ajax({
                url:'/member/remove',
                type:'POST',
                data:data,
                success: function(res) {
                    if (res.data == '1') {
                        window.tableMember.api().ajax.reload();
                        $('.btn-delete-member').attr('disabled', 'disabled');
                        $('.data-member input:checkbox:checked').removeAttr('checked');
                    }else {
                        $('#modal-notif .block-content p').text(res.data);
                        $('#modal-notif').modal('show');
                    }
                },

                error: function(jqXHR, exception) {
                    $('#modal-notif .block-content p').text(jqXHR.status);
                    $('#modal-notif').modal('show');
                },
            });
        }
    }else {
        alert('Please select 1 or more member first!');
    }
});
