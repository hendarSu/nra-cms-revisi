/*
 *  Document   : base_pages_login.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Login Page
 */

var BasePagesLogin = function() {
    // Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationLogin = function() {
        jQuery('.js-validation-login').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                email: {
                    required: true,
                },
                password: {
                    required: true,
                },
            },
            messages: {
                email: {
                    required: 'Please enter a email',
                },
                password: {
                    required: 'Please provide a password',
                },
            },
            submitHandler: function(form) {
                var data = $(form).serialize();
                var action = $(form).attr('action');
                var button = $(form).find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('checking..');
                $.ajax({
                    url:action,
                    method:'POST',
                    data: data,
                    timeout: 10000,
                    success: function(res) {
                        if (res.data == '1') {
                            window.localStorage.clear();
                            document.location.href = '/dashboard';
                        }else {
                            $('#modal-notif .block-content p').text(res.data);
                            $('#modal-notif').modal('show');
                            button.removeAttr('disabled');
                            button.text('Log in');
                        }
                    },

                    error: function(jqXHR, exception) {
                        $('#modal-notif .block-content p').text(jqXHR.status);
                        $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Log in');
                    },
                });
            },
        });
    };

    return {
        init: function() {
            // Init Login Form Validation
            initValidationLogin();
        },
    };
}();

// Initialize when page loads
jQuery(function() { BasePagesLogin.init(); });
