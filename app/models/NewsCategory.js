'use strict';

const News = require('./News');

module.exports = bookshelf.Model.extend({
    tableName: 'news_categories',
    hasTimestamps: true,

    news: function() {
        return this.hasMany(News, 'category');
    },

    travel: function() {
       return this.belongsTo(Travel, 'travel_id');
   },

});
