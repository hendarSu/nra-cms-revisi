'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'serverKey',
    hasTimestamps: true,
});
