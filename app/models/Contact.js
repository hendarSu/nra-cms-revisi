'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'contacts',
    hasTimestamps: true,
});
