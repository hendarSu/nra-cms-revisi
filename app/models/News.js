'use strict';

const Travel = require('./Travel');
const Employee = require('./Employee');
const NewsCategory = require('./NewsCategory')

module.exports = bookshelf.Model.extend({
    tableName: 'news',
    hasTimestamps: true,

    travel: function() {
        return this.belongsTo(Travel, 'travel_id');
    },

    author: function() {
        return this.belongsTo(Employee, 'author');
    },

    category: function() {
        return this.belongsTo(NewsCategory, 'category_id');
    },

});
