'use strict';

const Member = require('./Member');

module.exports = bookshelf.Model.extend({
    tableName: 'sponsors',
    hasTimestamps: true,

    member: function() {
        return this.hasMany(Member, 'memeber_id');
    },
    
});
