'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'members',
    hasTimestamps: true,

    travel: function() {
       return this.belongsTo(Travel, 'travel_id');
   },
});
