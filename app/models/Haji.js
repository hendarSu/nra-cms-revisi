'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'hajji',
    hasTimestamps: true,

    travel: function() {
       return this.belongsTo(Travel, 'travel_id');
   },
});
