'use strict';

const Travel = require('./Travel');
const Album = require('./Album');

module.exports = bookshelf.Model.extend({
  tableName: 'medias',
  hasTimestamps: true,

  album: function() {
    return this.belongsTo(Album, 'album_id');
  },

  travel: function() {
    return this.belongsTo(Travel, 'travel_id');
  },

});
