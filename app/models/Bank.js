'use strict';

const Travel = require('./Travel');

module.exports = bookshelf.Model.extend({
    tableName: 'banks',
    hasTimestamps: true,

      travel: function() {
        return this.hasMany(Travel, 'id');
    },
});
