'use strict';

const Travel = require('./Travel');
// const PromotionCheckpoint = require('./PromotionCheckpoint');
// const PromotionDepart = require('./PromotionDepart');

module.exports = bookshelf.Model.extend({
    tableName: 'promotions',
    hasTimestamps: true,

    travel: function() {
        return this.belongsTo(Travel, 'travel_id');
    },
});
