'use strict';

const Media = require('./Media');

module.exports = bookshelf.Model.extend({
    tableName: 'albums',
    hasTimestamps: true,

    media: function() {
        return this.hasMany(Media, 'album_id');
    },

    travel: function() {
       return this.belongsTo(Travel, 'travel_id');
   },

});
