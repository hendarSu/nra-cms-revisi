'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'token',
    hasTimestamps: true,
});
