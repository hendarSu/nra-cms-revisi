'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'employes',
    hasTimestamps: true,

    travel: function() {
       return this.belongsTo(Travel, 'travel_id');
   },
});
