'use strict';

const Travel = require('./Travel');

module.exports = bookshelf.Model.extend({
    tableName: 'management_greetings',
    hasTimestamps: true,

     travel: function() {
        return this.belongsTo(Travel, 'travel_id');
    },

});
