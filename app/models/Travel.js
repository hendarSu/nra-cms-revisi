'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'travels',
    hasTimestamps: true,
});
