var navigation = {
    admin: [
        {
            url: '/dashboard',
            displayName: 'Dashboard',
            child: false,
            heading: false,
            icon: 'si-speedometer',
        },
        {
            url: '#',
            displayName: 'Apps Menu',
            child: false,
            heading: true,
            icon: '',
        },
        {
            url: '/employees',
            displayName: 'Manage Employee',
            child: false,
            heading: false,
            icon: 'si-user-following',
        },

        {
            url: '/travel',
            displayName: 'Travel',
            child: false,
            heading: false,
            icon: 'si-docs',
        },

        {
            url: '/contact',
            displayName: 'Contact',
            child: false,
            heading: false,
            icon: 'si-speech',
        },
        // {
        //     url: '',
        //     displayName: 'Profile Taravel',
        //     child: [{
        //         url: '/profile/profile-Umum',
        //         displayName: 'Profile Umum',
        //         child: false,
        //         icon: 'si-docs',
        //     },
        //     {
        //         url: '/Profile/Profile-Alamat',
        //         displayName: 'Profile Alamat Perusahaan',
        //         child: false,
        //         icon: 'si-docs',
        //     },
        //     {
        //         url: '/Profile/Mitra-Perbankan',
        //         displayName: 'Mitra Perbankan',
        //         child: false,
        //         icon: 'si-docs',
        //     }, ],
        //     icon: 'si-docs',
        // },

        {
            url: '',
            displayName: 'News',
            child: [{
                url: '/news/news_category',
                displayName: 'News Category',
                child: false,
                icon: 'si-docs',
            },
            {
                 url: '/news',
                displayName: 'News',
                child: false,
                icon: 'si-docs',
            },
            // {
            //      url: '/Berita/pengumuman',
            //     displayName: 'Pengumuman (untuk Anggota)',
            //     child: false,
            //     icon: 'si-docs',
            // },
           ],
            icon: 'si-docs',
        },

        {
            url: '',
            displayName: 'Promotion',
            child: [{
                url: '/promotion',
                displayName: 'Promotions',
                child: false,
                icon: 'si-docs',
            },
            ],
            icon: 'si-docs',
        },

        {
            url: '',
            displayName: 'Ibadah',
            child: [{
                url: '/ibadah/haji',
                displayName: 'Ibadah Haji',
                child: false,
                icon: 'si-docs',
            },
            {
                 url: '/ibadah/umrah',
                displayName: 'Ibadah Umrah',
                child: false,
                icon: 'si-docs',
            }, ],
            icon: 'si-book-open',
        },

        {
            url: '',
            displayName: 'Gallery',
            child: [{
                url: '/gallery/albums',
                displayName: 'Albums',
                child: false,
                icon: 'si-picture',
            },
            {
                 url: '/gallery/medias',
                displayName: 'Medias',
                child: false,
                icon: 'si-picture',
            }, ],
            icon: 'si-picture',
        },

        {
            url: '/testimonial',
            displayName: 'Testimonial',
            child: false,
            heading: false,
            icon: 'si-speech',
        },

        {
            url: '',
            displayName: 'Member Management',
            child: [
            {
                 url: '/members',
                displayName: 'Members',
                child: false,
                icon: 'si-users',
            }, ],
            icon: 'si-user',
        },

    ],
};
global.Navigations = navigation;
