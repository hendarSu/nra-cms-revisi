'use strict';

const Member = require('./Member');
const Employee = require('./Employee');

module.exports = bookshelf.Model.extend({
    tableName: 'approval',
    hasTimestamps: true,

    member: function() {
        return this.belongsTo(Member, 'member_id');
    },

     employee : function() {
        return this.belongsTo(Employee, 'employee_id');
    },

});
