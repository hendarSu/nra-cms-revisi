<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>NRA CMS</title>
        {# <title>{% block title %}{% endblock %}</title> #}

        <meta name="description" content="OneUI - Admin Dashboard Template & UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->

        <link rel="shortcut icon" type="image/png" href="/img/logo.png">

        <link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="/js/plugins/slick/slick.min.css">
        <link rel="stylesheet" href="/js/plugins/slick/slick-theme.min.css">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="/css/oneui.css">

        <!-- my steyle css -->
        <link rel="stylesheet" href="/css/style.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <link rel="stylesheet" id="css-theme" href="/css/themes/flat.min.css">
        <link rel="stylesheet" href="/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
        {% block styles %}{% endblock %}
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available Classes:

            'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
        -->
        <div id="page-container" class="sidebar-l sidebar-o side-scroll side-overlay-o header-navbar-fixed enable-cookies">
            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <a class="h5 text-white" href="/dashboard">
                                <span class="sidebar-mini-show">
                                    <!-- <img src="/img/logo.png" width="30" style="position: absolute; top:19px; left: 13px;"> -->
                                </span>

                                <span class="sidebar-mini-hide">
                                {# <h1>ARSEN</h1> #}
                                    <img src="/img/toolbar-logo.png" height="37" style="position:absolute; top:15px;  left: 50px;">
                                </span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                            {% for nav in Navigations['admin'] %}
                                {% if nav.heading %}
                                    <li class="nav-main-heading">
                                        <span class="sidebar-mini-hide">{{nav.displayName}}</span>
                                    </li>
                                {% elseif !nav.child %}
                                    <li>
                                        <a class="" href="{{nav.url}}">
                                            <i class="si {{nav.icon}}"></i>
                                            <span class="sidebar-mini-hide">{{nav.displayName}}</span>
                                        </a>
                                    </li>
                                {% else %}
                                    <li>
                                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                                            <i class="si {{nav.icon}}"></i>
                                            <span class="sidebar-mini-hide">{{nav.displayName}}</span></a>
                                            <ul>
                                                {% for navChild in nav.child %}
                                                    <li>
                                                        <a href="{{navChild.url}}">{{navChild.displayName}}</a>
                                                    </li>
                                                {% endfor %}
                                            </ul>
                                </li>
                                {% endif %}
                            {% endfor %}
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                                <img src="{{user.picture}}" alt="Avatar">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-header">Account</li>
                                <li>
                                    <a tabindex="-1" href="/admins/profile">
                                        <i class="si si-user pull-right"></i>Profile
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a tabindex="-1" href="/logout">
                                        <i class="si si-logout pull-right"></i>Log out
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
                    <li class="hidden-md hidden-lg">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <!-- <h3>DUNIA SELEBAR DAUN KELOR</h3> -->
                    </li>
                </ul>
                <!-- END Header Navigation Left -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                {% block body %}{% endblock %}
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-right">
                    Created with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://minova.id" target="_blank">minova</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="" target="_blank">NRA {{versionApp}}</a> &copy; <span class="js-year-copy"></span>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="/js/core/jquery.min.js"></script>
        <script src="/js/core/bootstrap.min.js"></script>
        <script src="/js/core/jquery.slimscroll.min.js"></script>
        <script src="/js/core/jquery.scrollLock.min.js"></script>
        <script src="/js/core/jquery.appear.min.js"></script>
        <script src="/js/core/jquery.countTo.min.js"></script>
        <script src="/js/core/jquery.placeholder.min.js"></script>
        <script src="/js/core/js.cookie.min.js"></script>
        <script src="/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="/js/app.js"></script>

        <!-- Page Plugins -->
        <script src="/js/plugins/slick/slick.min.js"></script>
        <script src="/js/plugins/chartjs/Chart.min.js"></script>
        <script src="/moment/moment.js"></script>

        {% block scripts %}{% endblock %}
        <script>
            $(function () {
                // Init page helpers (Slick Slider plugin)
                App.initHelpers(['slick', 'datepicker']);

                var path = window.location.href;
                $('.nav-main li a').each(function(){
                    path = path.replace('#', '');
                    if(this.href === path){
                        $(this).addClass('active');
                    }
                });

                var checkCookie = localStorage.getItem("kelor");
                if (checkCookie != null) {
                    $('.nav-main > li > a:eq('+checkCookie+')').parent('li').addClass('open').next().show();
                }

                $('.nav-main > li > a').click(function(){
                    var navIndex = $('.nav-main > li > a').index(this);
                    localStorage.setItem("kelor", navIndex);
                });
            });
        </script>

        <!-- modals notif-->
        <div class="modal fade" id="modal-notif" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout modal-sm">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-danger">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Warning</h3>
                        </div>
                        <div class="block-content">
                            <p></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm" type="button" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>