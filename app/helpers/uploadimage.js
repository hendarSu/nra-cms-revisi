'use strict';

const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');

module.exports = {
    fromFile: Async.make(function *(prefix, file, write) {
        if (file.type.indexOf('image') !== 0) {
            throw error('Not a valid image type', 400);
        }

        const splittedName = file.name.split('.');
        const extension = splittedName[splittedName.length - 1];
        const withoutExt = `${prefix}_${splittedName.slice(0, splittedName.length - 1).join('')}_${Date.now()}`;
        const newName = `${withoutExt}.${extension}`;
        const newPath = path.join(config.uploadDir, newName);

        if (write) {
            const writeStream = fs.createWriteStream(newPath);
            const readStream = fs.createReadStream(file.path);

            readStream.pipe(writeStream);
        }

        // yield fs.unlinkAsync(file.path);
        return newName;
    }),

    base64: Async.make(function *(prefix, pathUpload, base64string) {
        const validationRe = /^data:image\/(png|jpeg);base64,/;

        if (!validationRe.test(base64string)) throw error('Not a valid image type', 400);

        const withoutExt = `${prefix}_${Utils.uid(32)}_${Date.now()}`;
        const newFilename = `${withoutExt}.png`;
        const newBase64String = base64string.replace(/^data:image\/(png|jpeg);base64,/, '');
        // const newPath = path.join(config.uploadDir, newFilename);
        const newPath = path.join(pathUpload, newFilename);

        yield fs.writeFileAsync(newPath, newBase64String, 'base64').catch(console.error);

        try {
            const stats = yield fs.statAsync(newPath);
        } catch (ex) {
            console.error(ex);
            return false;
        }

        // yield fs.unlinkAsync(newPath);
        return newFilename;
    }),
};
