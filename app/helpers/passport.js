'use strict';

const passport = require('passport');
const Promise = require('bluebird');

passport.serializeUser((user, done) => {
    // done(null, user);
    done(null, user.id);
});

passport.deserializeUser(wrap(function *(id, done) {
    var user = yield Employee.where({id: id}).fetch().catch(console.error);
    if (!_.isEmpty(user)) {
        user = user.toJSON();
        delete user.password;
        done(null, user);
    } else {
        done(new Error('Invalid login data'));
    }
}));

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(id, done) {
        cr(id, done).catch(console.error);
    };
}
