
const express = require('express');
const router = express.Router();

var auth = require('../middlewares/authenticated');
var multipart = require('../middlewares/multipart');

// Index
router.get('/', AuthenticationController.index);
router.get('/dashboard', /*auth.isAuthenticated(),*/ DashboardController.index);
// router.get('/Profile/Profile-Umum', /*auth.isAuthenticated(),*/ ProfileUmumController.index);
// router.get('/Profile/Profile-Alamat', /*auth.isAuthenticated(),*/ ProfileAlamatPerusahaanController.index);
// router.get('/Profile/Mitra-Perbankan', /*auth.isAuthenticated(),*/ BankController.index);
router.get('/news', /*auth.isAuthenticated(),*/ NewsController.index);
// router.get('/Berita/Kategori-Berita', /*auth.isAuthenticated(),*/ KategoriBeritaController.index);
router.get('/news/news_category', /*auth.isAuthenticated(),*/ NewsCategoryController.index);

// haji
router.get('/ibadah/haji', /*auth.isAuthenticated(),*/ HajiController.index);
router.post('/haji/update/:id', /*auth.isAuthenticated(),*/ HajiController.update);

// umrah
router.get('/ibadah/umrah', /*auth.isAuthenticated(),*/ UmrahController.index);
router.post('/umrah/update/:id', /*auth.isAuthenticated(),*/ UmrahController.update);

// router.get('/promosi', /*auth.isAuthenticated(),*/ PromosiController.index);
router.get('/testimonial', /*auth.isAuthenticated(),*/ TestimonialController.index);


router.get('/travel', /*auth.isAuthenticated(),*/ TravelController.index);
router.post('/travel/update/:id', /*auth.isAuthenticated(),*/ TravelController.update);


// Auth
router.post('/auth/login', AuthenticationController.login);
router.get('/auth/logout', AuthenticationController.logout);
router.get('/forgot/:email?', AuthenticationController.forgot);
router.post('/auth/forgot', AuthenticationController.forgotProcess);


//employee
router.get('/employees', /*auth.isAuthenticated(),*/ EmployeeController.index);
router.get('/employees/get', /*auth.isAuthenticated(),*/ EmployeeController.get);
router.post('/employees/add', /*auth.isAuthenticated(),*/ EmployeeController.add);
router.post('/employees/remove', /*auth.isAuthenticated(),*/ EmployeeController.remove);
router.get('/employees/get/:id', EmployeeController.dataSingle);
router.post('/employees/update/:id', EmployeeController.update);

// news categories

router.get('/categories/get', /*auth.isAuthenticated(),*/ NewsCategoryController.get);
router.post('/categories/add', /*auth.isAuthenticated(),*/ NewsCategoryController.add);
router.post('/categories/remove', /*auth.isAuthenticated(),*/ NewsCategoryController.remove);
router.get('/categories/get/:id', NewsCategoryController.dataSingle);
router.post('/categories/update/:id', NewsCategoryController.update);


// travel
// router.get('/travel/get/:id', TravelController.dataSingle);
// router.post('/travel/update/:id', TravelController.update);

// testimonial
router.get('/testimonial/get', TestimonialController.get);
router.get('/testimonial/get/:id', TestimonialController.dataSingle);
router.post('/testimonial/update/:id', TestimonialController.update);
//member

router.get('/members',MemberController.index);
router.get('/members/get',MemberController.get);
// news

router.get('/news/get', NewsController.get);
router.post('/news/add', NewsController.add);
router.post('/news/remove', NewsController.remove);
router.get('/news/get/:id', NewsController.dataSingle);
router.post('/news/update/:id', NewsController.update);

// contact
router.get('/contact', ContactController.index);
router.get('/contact/get', ContactController.get);
router.post('/contact/add', ContactController.add);
router.post('/contact/remove', ContactController.remove);
router.post('/contact/update/:id', ContactController.update);
router.get('/contact/get/:id', ContactController.dataSingle);

// promorion
router.get('/promotion',PromotionController.index);
router.get('/promotion/get',PromotionController.get);
router.post('/promotion/add',PromotionController.add);
router.post('/promotion/remove',PromotionController.remove);
router.post('/promotion/update/:id',PromotionController.update);
router.get('/promotion/get/:id', PromotionController.dataSingle);

// albums
router.get('/gallery/albums',AlbumController.index);
router.get('/album/get',AlbumController.get);
router.post('/album/add',AlbumController.add);
router.post('/album/remove',AlbumController.remove);
router.post('/album/update/:id',AlbumController.update);
router.get('/album/get/:id', AlbumController.dataSingle);

// medias
router.get('/gallery/medias',MediaController.index);
router.get('/media/get',MediaController.get);
router.post('/media/add',MediaController.add);
router.post('/media/remove',MediaController.remove);
router.post('/media/update/:id',MediaController.update);
router.get('/media/get/:id', MediaController.dataSingle);

module.exports = router;
