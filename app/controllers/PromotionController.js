'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = '../uploads/promotion';
const pathDb = 'http://'+ config.hostname + '/uploads/promotion/';

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/promotion', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Promotion, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    add: Async.route(function *(req, res, next) {
        req.body.image = '';
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const promotionImagePath = yield upload.base64('promotion', pathUpload, req.body.srcDataCrop);

            if (!promotionImagePath) throw error('Failed to save promotion image', 500);
            req.body.image = path.join(pathDb, promotionImagePath);
        }

        const temp = yield new Promotion().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const tempJson = temp.toJSON();

        const id = parseInt(tempJson.max) + 1;
        req.body.id = id;

        delete req.body.srcDataCrop;
        const promotion = Promotion.forge();
        const added = yield promotion.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add promotion');
        }

        return res.ok('1');
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var promotion =  yield Promotion.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(promotion.toJSON());
   }),

   update: Async.route(function *(req, res, next) {

      var promotion1 = yield Promotion.forge({id: req.params.id}).fetch().catch(console.error);
      const updated = yield promotion1.save(req.body).catch(console.error);
      if (_.isEmpty(updated)) {
          return res.ok('Failed to update promotion');
      }

      return res.ok('1');
  }),

   remove: Async.route(function *(req, res, next) {

       if (req.body.promotionId.constructor === Array) {
           _.forEach(req.body.promotionId, wrap(function *(v, i) {

               var promotion = yield Promotion.forge({id: v}).fetch().catch(console.error);
               var promotiondata = promotion.toJSON();

               const deleted = yield promotion.destroy().catch(console.error);
               if (!_.isEmpty(deleted)) {
                   return res.ok('delete promotion Failed');
               }
           }));

           return res.ok('1');

       }else {

           var promotion = yield Promotion.forge({id: req.body.promotionId}).fetch().catch(console.error);
           var promotiondata = promotion.toJSON();
           const deleted = yield promotion.destroy().catch(console.error);
           if (!_.isEmpty(deleted)) {
              return res.ok('1');
           }

           return res.ok('1');
       }
   }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
