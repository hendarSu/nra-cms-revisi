'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/contact', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Contact, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var contacts =  yield Contact.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(contacts.toJSON());
   }),


    add: Async.route(function *(req, res, next) {


        const temp = yield new Contact().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const tempJson = temp.toJSON();
        const id = parseInt(tempJson.max) + 1;
        // req.body.id = id;

        const contact = Contact.forge();
        const added = yield contact.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add contact');
        }

        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {

        if (req.body.contactId.constructor === Array) {
            _.forEach(req.body.contactId, wrap(function *(v, i) {

                var contacts = yield Contact.forge({id: v}).fetch().catch(console.error);
                var contactData = contacts.toJSON();

                const deleted = yield contacts.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete contact Failed');
                }
            }));

            return res.ok('1');

        }else {

            var contacts = yield Contact.forge({id: req.body.contactId}).fetch().catch(console.error);
            var contactData = contacts.toJSON();
            const deleted = yield contacts.destroy().catch(console.error);
            if (!_.isEmpty(deleted)) {

            }

            return res.ok('1');
        }
    }),

    update: Async.route(function *(req, res, next) {

       var contacts = yield Contact.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield contacts.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update Employees');
       }

       return res.ok('1');
   }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
