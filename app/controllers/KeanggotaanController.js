'use strict';
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

// const pathUpload = '../uploads/employees';
// const pathDb = 'http://'+ config.hostname + '/uploads/employees/';

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/keanggotaan', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Bank, 'datatables').forge()
        .paginate({
            request: req,
            withRelated: ['travel'],
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

     dataSingle: Async.route(function *(req, res, next) {
        var banks =  yield Bank.where({id: req.params.id}).fetch().catch(console.error);
        res.ok(banks.toJSON());
    }),


    add: Async.route(function *(req, res, next) {

        const temp = yield new Bank().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const bank1Json = temp.toJSON();

        const id = parseInt(bank1Json.max) + 1;
        // req.body.id = id;

        var banks = Bank.forge();
        // req.body.banks.tarvel_id = 1;
        const added = yield banks.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add bank');
        }

        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {

        if (req.body.bankId.constructor === Array) {
            _.forEach(req.body.bankId, wrap(function *(v, i) {

                var banks = yield Bank.forge({id: v}).fetch().catch(console.error);
                var bankdata = banks.toJSON();

                const deleted = yield banks.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete bank Failed');
                }
            }));

            return res.ok('1');

        }else {

            var banks = yield Bank.forge({id: req.body.bankId}).fetch().catch(console.error);
            var bankdata = banks.toJSON();
            const deleted = yield banks.destroy().catch(console.error);
            if (!_.isEmpty(deleted)) {

            }

            return res.ok('1');
        }
    }),

     update: Async.route(function *(req, res, next) {

        var banks = yield Bank.forge({id: req.params.id}).fetch().catch(console.error);
        const updated = yield banks.save(req.body).catch(console.error);
        if (_.isEmpty(updated)) {
            return res.ok('Failed to update Banks');
        }

        return res.ok('1');
    }),



};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
