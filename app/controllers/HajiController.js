'use strict';
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

// const pathUpload = '../uploads/employees';
// const pathDb = 'http://'+ config.hostname + '/uploads/employees/';

module.exports = {

  index: Async.route(function *(req, res, next) {
      const model = res.model;

      const hajis = yield Haji.forge('travel_id',1).fetch().catch(console.error);
    model.hajis = hajis.toJSON();


      res.render('content/ibadahHaji', model);
  }),

  update: Async.route(function *(req, res, next) {

     var hajis = yield Haji.forge({id: req.params.id}).fetch().catch(console.error);
     const updated = yield hajis.save(req.body).catch(console.error);
     if (_.isEmpty(updated)) {
         return res.ok('Failed to update haji');
     }

     return res.ok('Update Ibadah Haji success');
 }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
