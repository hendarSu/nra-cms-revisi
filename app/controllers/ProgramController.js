'use strict';
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

// const pathUpload = '../uploads/employees';
// const pathDb = 'http://'+ config.hostname + '/uploads/employees/';

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/program', model);
    }),

    // get: Async.route(function *(req, res, next) {
    //     pm(Employee, 'datatables').forge()
    //     .paginate({
    //         request: req,
    //     })
    //     .query(function(qb) {
    //         // qb.where('id', '!=', req.user.id);
    //     })
    //     .end()
    //     .then(function(results) {
    //         _.forEach(results.data, function(v, i) {
    //             delete results.data[i].password;
    //         });

    //         res.send(results);
    //     });
    // }),

    // add: Async.route(function *(req, res, next) {
    //     req.body.photo = '';
    //     if (!_.isEmpty(req.body.srcDataCrop)) {
    //         const employeeImagePath = yield upload.base64('employee', pathUpload, req.body.srcDataCrop);

    //         if (!employeeImagePath) throw error('Failed to save employee photo', 500);
    //         req.body.photo = path.join(pathDb, employeeImagePath);
    //     }

    //     const temp = yield new Employee().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
    //     const tempJson = temp.toJSON();
    //     const id = parseInt(tempJson.max) + 1;
    //     req.body.id = id;

    //     delete req.body.srcDataCrop;
    //     delete req.body.repassword;
    //     var passHash = bcrypt.hashSync(req.body.password, 8);
    //     req.body.password = passHash;
    //     const employee = Employee.forge();
    //     const added = yield employee.save(req.body).catch(console.error);
    //     if (_.isEmpty(added)) {

    //         return res.ok('Failed to add employee');
    //     }

    //     return res.ok('1');
    // }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
