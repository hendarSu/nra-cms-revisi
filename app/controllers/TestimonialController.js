'use strict';
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

// const pathUpload = '../uploads/employees';
// const pathDb = 'http://'+ config.hostname + '/uploads/employees/';

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/testimonial', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Testimonial, 'datatables').forge()
        .paginate({
            request: req,
            withRelated: ['travel'],
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var tes =  yield Testimonial.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(tes.toJSON());
   }),

    update: Async.route(function *(req, res, next) {

       var tes = yield Testimonial.forge({id: req.params.id}).fetch().catch(console.error);
       const updated = yield tes.save(req.body).catch(console.error);
       if (_.isEmpty(updated)) {
           return res.ok('Failed to update news');
       }

       return res.ok('1');
   }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
