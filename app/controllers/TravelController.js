'use strict';
const Promise = require('bluebird');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

  index: Async.route(function *(req, res, next) {
      const model = res.model;

      const travels = yield Travel.forge('id',1).fetch().catch(console.error);
      model.travels = travels.toJSON();

      res.render('content/travel', model);
  }),

     update: Async.route(function *(req, res, next) {

        var travels = yield Travel.forge({id: req.params.id}).fetch().catch(console.error);
        const updated = yield travels.save(req.body).catch(console.error);
        if (_.isEmpty(updated)) {
            return res.ok('Failed to update travels');
        }

        return res.ok('Update travel success');
    }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
