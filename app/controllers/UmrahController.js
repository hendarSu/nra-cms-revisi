'use strict';
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');


module.exports = {

  index: Async.route(function *(req, res, next) {
    const model = res.model;

    const umrahs = yield Umrah.forge('travel_id',1).fetch().catch(console.error);
    model.umrahs = umrahs.toJSON();


    res.render('content/ibadahUmrah', model);
  }),

  update: Async.route(function *(req, res, next) {

     var umrahs = yield Umrah.forge({id: req.params.id}).fetch().catch(console.error);
     const updated = yield umrahs.save(req.body).catch(console.error);
     if (_.isEmpty(updated)) {
         return res.ok('Failed to update Umrah');
     }

     return res.ok('Update Ibadah Umrah success');
 }),


};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
