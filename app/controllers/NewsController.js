'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = '../uploads/news';
const pathDb = 'http://'+ config.hostname + '/uploads/news/';

module.exports = {

  index: Async.route(function *(req, res, next) {
      var model = res.model;
      const categories = yield NewsCategories.forge().query(function(categories){
        categories.where('travel_id', 1);
      }).fetch().catch(console.error);

      model.categories = categories.toJSON();
      console.log(model);

      res.render('content/news', model);
  }),

    get: Async.route(function *(req, res, next) {
        pm(News, 'datatables').forge()
        .paginate({
            request: req,
            withRelated: ['author','category'],
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    add: Async.route(function *(req, res, next) {
        req.body.image = '';
        if (!_.isEmpty(req.body.srcDataCrop)) {
            const newsImagePath = yield upload.base64('news', pathUpload, req.body.srcDataCrop);

            if (!newsImagePath) throw error('Failed to save news image', 500);
            req.body.image = path.join(pathDb, newsImagePath);
        }

        const temp = yield new News().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const tempJson = temp.toJSON();

        const id = parseInt(tempJson.max) + 1;
        // req.body.id = id;

        delete req.body.srcDataCrop;
        const news = News.forge();
        const added = yield news.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add news');
        }

        return res.ok('1');
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var news =  yield News.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(news.toJSON());
   }),

   update: Async.route(function *(req, res, next) {

      var news1 = yield News.forge({id: req.params.id}).fetch().catch(console.error);
      const updated = yield news1.save(req.body).catch(console.error);
      if (_.isEmpty(updated)) {
          return res.ok('Failed to update news');
      }

      return res.ok('1');
  }),

   remove: Async.route(function *(req, res, next) {

       if (req.body.newsId.constructor === Array) {
           _.forEach(req.body.newsId, wrap(function *(v, i) {

               var news = yield News.forge({id: v}).fetch().catch(console.error);
               var newsdata = news.toJSON();

               const deleted = yield news.destroy().catch(console.error);
               if (!_.isEmpty(deleted)) {
                   return res.ok('delete news Failed');
               }
           }));

           return res.ok('1');

       }else {

           var news = yield News.forge({id: req.body.newsId}).fetch().catch(console.error);
           var newsdata = news.toJSON();
           const deleted = yield news.destroy().catch(console.error);
           if (!_.isEmpty(deleted)) {
              return res.ok('1');
           }

           return res.ok('1');
       }
   }),



};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
