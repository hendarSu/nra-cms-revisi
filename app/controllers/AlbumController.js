'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

// const pathUpload = '../uploads/employees';
// const pathDb = 'http://'+ config.hostname + '/uploads/employees/';

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/albums', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(Album, 'datatables').forge()
        .paginate({
            request: req,
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var albums =  yield Album.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(albums.toJSON());
   }),


    add: Async.route(function *(req, res, next) {

        const temp = yield new Album().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const album1Json = temp.toJSON();

        const id = parseInt(album1Json.max) + 1;
        // req.body.id = id;

        var albums = Album.forge();
        // req.body.albums.tarvel_id = 1;
        const added = yield albums.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add albums');
        }

        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {

        if (req.body.albumId.constructor === Array) {
            _.forEach(req.body.albumId, wrap(function *(v, i) {

                var albums = yield Album.forge({id: v}).fetch().catch(console.error);
                var categoriesdaata = albums.toJSON();

                const deleted = yield albums.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete album Failed');
                }
            }));

            return res.ok('1');

        }else {

            var albums = yield Album.forge({id: req.body.albumId}).fetch().catch(console.error);
            var categoriesdaata = albums.toJSON();
            const deleted = yield albums.destroy().catch(console.error);
            if (!_.isEmpty(deleted)) {

            }

            return res.ok('1');
        }
    }),

     update: Async.route(function *(req, res, next) {

        var albums = yield Album.forge({id: req.params.id}).fetch().catch(console.error);
        const updated = yield albums.save(req.body).catch(console.error);
        if (_.isEmpty(updated)) {
            return res.ok('Failed to update News Categories');
        }

        return res.ok('1');
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
