'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

const pathUpload = '../uploads/media';
const pathDb = 'http://'+ config.hostname + '/uploads/media/';

module.exports = {

  index: Async.route(function *(req, res, next) {
      var model = res.model;
      const albums = yield Albums.forge().query(function(albums){
        albums.where('travel_id', 1);
      }).fetch().catch(console.error);
      model.albums = albums.toJSON();
      console.log(model);

      res.render('content/media', model);
  }),

    get: Async.route(function *(req, res, next) {
        pm(Media, 'datatables').forge()
        .paginate({
            request: req,
            withRelated: ['album'],
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    add: Async.route(function *(req, res, next) {

        const temp = yield new Media().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const tempJson = temp.toJSON();

        const id = parseInt(tempJson.max) + 1;
        req.body.id = id;

        delete req.body.srcDataCrop;
        const media = Media.forge();
        const added = yield media.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add media');
        }

        return res.ok('1');
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var media =  yield Media.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(media.toJSON());
   }),

   update: Async.route(function *(req, res, next) {

      var media1 = yield Media.forge({id: req.params.id}).fetch().catch(console.error);
      const updated = yield media1.save(req.body).catch(console.error);
      if (_.isEmpty(updated)) {
          return res.ok('Failed to update media');
      }

      return res.ok('1');
  }),

   remove: Async.route(function *(req, res, next) {

       if (req.body.mediaId.constructor === Array) {
           _.forEach(req.body.mediaId, wrap(function *(v, i) {

               var media = yield Media.forge({id: v}).fetch().catch(console.error);
               var mediadata = media.toJSON();

               const deleted = yield media.destroy().catch(console.error);
               if (!_.isEmpty(deleted)) {
                   return res.ok('delete media Failed');
               }
           }));

           return res.ok('1');

       }else {

           var media = yield Media.forge({id: req.body.mediaId}).fetch().catch(console.error);
           var mediadata = media.toJSON();
           const deleted = yield media.destroy().catch(console.error);
           if (!_.isEmpty(deleted)) {
              return res.ok('1');
           }

           return res.ok('1');
       }
   }),



};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
