'use strict';

const Promise = require('bluebird');
const upload = require('../helpers/uploadimage');
const pm = require('bookshelf-pagemaker')(bookshelf);
const bcrypt = require('bcryptjs');
const path = require('path');

// const pathUpload = '../uploads/employees';
// const pathDb = 'http://'+ config.hostname + '/uploads/employees/';

module.exports = {

    index: Async.route(function *(req, res, next) {
        const model = res.model;
        res.render('content/newsCategory', model);
    }),

    get: Async.route(function *(req, res, next) {
        pm(NewsCategory, 'datatables').forge()
        .paginate({
            request: req,
            withRelated: ['travel'],
        })
        .query(function(qb) {
            // qb.where('id', '!=', req.user.id);
        })
        .end()
        .then(function(results) {
            _.forEach(results.data, function(v, i) {
                delete results.data[i].password;
            });

            res.send(results);
        });
    }),

    dataSingle: Async.route(function *(req, res, next) {
       var categories =  yield NewsCategory.where({id: req.params.id}).fetch().catch(console.error);
       res.ok(categories.toJSON());
   }),


    add: Async.route(function *(req, res, next) {

        const temp = yield new NewsCategory().query(function(qPC) { qPC.max('id'); }).fetch().catch(console.error);;
        const category1Json = temp.toJSON();

        const id = parseInt(category1Json.max) + 1;
        req.body.id = id;

        var categories = NewsCategory.forge();
        // req.body.categories.tarvel_id = 1;
        const added = yield categories.save(req.body).catch(console.error);
        if (_.isEmpty(added)) {

            return res.ok('Failed to add category');
        }

        return res.ok('1');
    }),

    remove: Async.route(function *(req, res, next) {

        if (req.body.categoryId.constructor === Array) {
            _.forEach(req.body.categoryId, wrap(function *(v, i) {

                var categories = yield NewsCategory.forge({id: v}).fetch().catch(console.error);
                var categoriesdaata = categories.toJSON();

                const deleted = yield categories.destroy().catch(console.error);
                if (!_.isEmpty(deleted)) {
                    return res.ok('delete category Failed');
                }
            }));

            return res.ok('1');

        }else {

            var categories = yield NewsCategory.forge({id: req.body.categoryId}).fetch().catch(console.error);
            var categoriesdaata = categories.toJSON();
            const deleted = yield categories.destroy().catch(console.error);
            if (!_.isEmpty(deleted)) {

            }

            return res.ok('1');
        }
    }),

     update: Async.route(function *(req, res, next) {

        var categories = yield NewsCategory.forge({id: req.params.id}).fetch().catch(console.error);
        const updated = yield categories.save(req.body).catch(console.error);
        if (_.isEmpty(updated)) {
            return res.ok('Failed to update News Categories');
        }

        return res.ok('1');
    }),



};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
