'use strict';

const ServerKey = require('../models/ServerKey');

module.exports = bookshelf.Collection.extend({
    model: ServerKey,
});
