'use strict';

const Promotion = require('../models/Promotion');

module.exports = bookshelf.Collection.extend({
    model: Promotion,
});
