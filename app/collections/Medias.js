'use strict';

const Media = require('../models/Media');

module.exports = bookshelf.Collection.extend({
    model: Media,
});