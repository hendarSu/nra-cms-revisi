'use strict';

const Approval = require('../models/Approval');

module.exports = bookshelf.Collection.extend({
    model: Approval,
});