'use strict';

const Album = require('../models/Album');

module.exports = bookshelf.Collection.extend({
    model: Album,
});