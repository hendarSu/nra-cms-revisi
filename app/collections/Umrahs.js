'use strict';

const Umrah = require('../models/Umrah');

module.exports = bookshelf.Collection.extend({
    model: Umrah,
});
