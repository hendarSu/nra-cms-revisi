'use strict';

const Testimonial = require('../models/Testimonial');

module.exports = bookshelf.Collection.extend({
    model: Testimonial,
});