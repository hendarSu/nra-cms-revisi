'use strict';

const Travel = require('../models/Travel');

module.exports = bookshelf.Collection.extend({
    model: Travel,
});