'use strict';

const NewsCategory = require('../models/NewsCategory');

module.exports = bookshelf.Collection.extend({
    model: NewsCategory,
});
