'use strict';

const Bank = require('../models/Bank');

module.exports = bookshelf.Collection.extend({
    model: Bank,
});