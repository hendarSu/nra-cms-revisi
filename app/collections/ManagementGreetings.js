'use strict';

const ManagementGreeting = require('../models/ManagementGreeting');

module.exports = bookshelf.Collection.extend({
    model: ManagementGreeting,
});
