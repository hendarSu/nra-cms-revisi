'use strict';

const Member = require('../models/Member');

module.exports = bookshelf.Collection.extend({
    model: Member,
});