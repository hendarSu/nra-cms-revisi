'use strict';

const Contact = require('../models/Contact');

module.exports = bookshelf.Collection.extend({
    model: Contact,
});
