'use strict';

const Haji = require('../models/Haji');

module.exports = bookshelf.Collection.extend({
    model: Haji,
});
